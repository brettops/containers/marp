ARG CONTAINER_PROXY
FROM ${CONTAINER_PROXY}node:lts-bullseye

# hadolint ignore=DL3008
RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
        chromium \
        python3 \
        python3-pip \
        python3-setuptools \
        python3-wheel \
        rsync \
        wget \
    && rm -rf /var/lib/apt/list/*

COPY requirements.txt /

# hadolint ignore=DL3013
RUN pip3 install --no-cache-dir -r /requirements.txt

# hadolint ignore=DL3016
RUN npm install -g @marp-team/marp-cli \
    && marp --version

COPY marp-format /usr/local/bin/

CMD ["/bin/bash"]
