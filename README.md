# Marp Container

<!-- BADGIE TIME -->

[![brettops container](https://img.shields.io/badge/brettops-container-209cdf?labelColor=162d50)](https://brettops.io)
[![pipeline status](https://gitlab.com/brettops/containers/marp/badges/main/pipeline.svg)](https://gitlab.com/brettops/containers/marp/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![code style: black](https://img.shields.io/badge/code_style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

Use the Marp CLI.

## Build & Run

With `make`:

```
make
```

With `docker` commands:

```
docker build -t marp .
docker run --rm -ti marp
```
